package com.casoftware.dao;

import com.casoftware.modal.User;

public interface UserDao extends Dao{

	 User getUser(String email);
	 
}
