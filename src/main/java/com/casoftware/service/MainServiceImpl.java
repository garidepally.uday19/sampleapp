package com.casoftware.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casoftware.dao.MainDao;
import com.casoftware.modal.User;

@Service("mainService")
@Transactional("transactionManager")
public class MainServiceImpl implements MainService {

	@Autowired
	MainDao mainDao;

	@Override
	public List<User> getAllUsers() {
		return mainDao.getAllUsers();
	}

}
