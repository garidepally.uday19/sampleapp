package com.casoftware.spring.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class CaSoftwareConfig {
	
	@Autowired
	private Environment environment;
	
	private static CaSoftwareConfig samvitConfig = null;
	
	public static CaSoftwareConfig getInstance() {
		return samvitConfig;
	}
	
	public String getConfigValue(String configKey) {
		return environment.getProperty(configKey);
	}
	
	@PostConstruct
	public void initIt() throws Exception {
		samvitConfig = this;
	}

}
