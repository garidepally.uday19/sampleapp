samvit.controller(
				"homeController",[
                 '$rootScope', 
                 '$scope', 
                 '$http',
                 '$q',
                 '$location',
                 'OAuth',
                 'OAuthToken',
                 '$route',
				function($rootScope, $scope, $http, $q, $location, OAuth,
						OAuthToken, $route) {

				$scope.initPage = function() {
						var locationPath = $location.path();

						var deferred = $q.defer();
						if (!OAuth.isAuthenticated()) {
							if (locationPath != null
									&&
									(
									locationPath.startsWith('/admin') 
									|| locationPath.startsWith('/manager')
									|| locationPath.startsWith('/accountant'))) {
								deferred.reject();
								$location.path('/login');
							} else
								deferred.resolve();
						} else {
							$http.post(
											'user/checkSessionOnPageRefresh',
											{
												headers : {
													"Content-Type" : "application/json",
													"Accept" : "application/json"
												}
											})
									.success(
											function(response, status, headers) {
												if (response.status == 'success') {
													var appLoginUser = response.result;
													$rootScope.appLoginUser = appLoginUser;
													if (appLoginUser.userType == 'admin') {
														$scope.$parent.headerTemplate = "views/admin/adminHeader.html";
														if (!locationPath.startsWith('/admin')) {
															deferred.resolve();
															$location.path("admin/home/");
														} else {
															deferred.resolve();
															$route.reload();
														}

													}else if (appLoginUser.userType == 'accountant') {
														$scope.$parent.headerTemplate = "views/accountant/accountantHeader.html";
														if (!locationPath.startsWith('/accountant')) {
															deferred.resolve();
															$location.path("accountant/home/");
														} else {
															deferred.resolve();
															$route.reload();
														}

													} else if (appLoginUser.userType == 'manager') {
														$scope.$parent.headerTemplate = "views/manager/managerHeader.html";
														if (!locationPath.startsWith('/manager')) {
															deferred.resolve();
															$location.path("manager/fleetManagement/");
														} else {
															deferred.resolve();
															$route.reload();
														}
													}
												} else {
													if (locationPath != null &&
															(locationPath.startsWith('/admin')
															 || locationPath.startsWith('/manager')
															 || locationPath.startsWith('/accountant'))) {
														deferred.resolve();
														$location.path('/index');
													} else
														deferred.resolve();
												}
											})
									.error(
											function(response, status, header,
													config) {
												if (locationPath.startsWith('/admin')
														|| locationPath.startsWith('/manager')
														|| locationPath.startsWith('/accountant')) {
													deferred.resolve();
													$location.path('/index');
												} else
													deferred.resolve();
											});
						}
						return deferred.promise;
					}
					}]);